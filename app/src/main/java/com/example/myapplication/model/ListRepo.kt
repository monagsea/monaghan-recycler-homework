package com.example.myapplication.model

import com.example.myapplication.model.remote.ListApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ListRepo : ListApi {

    private var appList = mutableListOf<String>()

//    private val ListApi = object : ListApi {
//        override suspend fun getList(): List<String> {
//            return appList
//        }
//
//        override suspend fun addList(input: String) {
//            appList.add(input)
//        }
//    }

    override suspend fun getList(): List<String> = withContext(Dispatchers.IO)
    {
        return@withContext appList
    }

    override suspend fun addList(input: String):Unit = withContext(Dispatchers.IO) {
        appList.add(input)
    }

    override suspend fun clearList(): Unit = withContext(Dispatchers.IO) {
        appList.clear()
    }
}