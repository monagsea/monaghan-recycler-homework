package com.example.myapplication.model.remote

interface ListApi {
    suspend fun getList(): List<String>
    suspend fun addList(input: String)
    suspend fun clearList()
}