package com.example.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.model.ListRepo
import kotlinx.coroutines.launch

class ListViewModel : ViewModel() {
    private val repo = ListRepo

    private val _list = MutableLiveData<List<String>>()
    val list: LiveData<List<String>> get() = _list

    fun getList() {
        viewModelScope.launch {
            val itemList = repo.getList()
            _list.value = itemList
        }
    }

    fun addToList(input: String) {
        viewModelScope.launch {
            repo.addList(input)
        }
    }

    fun clearList() {
        viewModelScope.launch {
            repo.clearList()
        }
    }

}