package com.example.myapplication.view

import android.R
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapters.ListRecyclerAdapter
import com.example.myapplication.databinding.FragmentListBinding
import com.example.myapplication.viewmodel.ListViewModel


class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!
    private val listViewModel by viewModels<ListViewModel>()




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun display() =
            with(binding.rvList) {
                layoutManager = LinearLayoutManager(this.context)
                adapter = ListRecyclerAdapter().apply {
                    listViewModel.getList()
                    listViewModel.list.observe(viewLifecycleOwner) {
                        addListItem(it)
                    }
                }
            }

        display()

        binding.etList.onFocusChangeListener =
            OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) binding.etList.hint = "" else binding.etList.hint =
                    "Write something here"
            }

        binding.etList.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                val input = binding.etList.text.toString()
                listViewModel.addToList(input.trim())
                display()
                binding.etList.text.clear()
                return@OnKeyListener true
            }
            false
        })

        binding.btnClear.setOnClickListener {
            listViewModel.clearList()
            display()
        }

        binding.btnList.setOnClickListener {
            val input = binding.etList.text.toString()
            listViewModel.addToList(input)
            display()
            binding.etList.text.clear()
        }
    }

}