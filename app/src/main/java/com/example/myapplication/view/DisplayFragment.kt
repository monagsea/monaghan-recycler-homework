package com.example.myapplication.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapters.DisplayRecyclerAdapter
import com.example.myapplication.databinding.FragmentDisplayBinding

class DisplayFragment : Fragment() {

    private var _binding: FragmentDisplayBinding? = null
    private val binding get() = _binding!!
//    private val colorViewModel by viewModels<ColorViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDisplayBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: DisplayFragmentArgs by navArgs()
        val item = args.string
        val splitString = item.split("")?.filter { num -> num.isNotEmpty() }?.toList()

        binding.rvLetterList.layoutManager = LinearLayoutManager(this.context)
        binding.rvLetterList.adapter = DisplayRecyclerAdapter().apply {
            splitString?.let { addNumbers(it) }
        }
    }
}