package com.example.myapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.ItemDisplayBinding

class DisplayRecyclerAdapter : RecyclerView.Adapter<DisplayRecyclerAdapter.DisplayViewHolder>() {
    private var displays = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisplayViewHolder {
        val binding = ItemDisplayBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DisplayViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DisplayViewHolder, position: Int) {
        val display = displays[position]
        holder.loadDisplay(display)
    }

    override fun getItemCount(): Int {
        return displays.size
    }

    fun addNumbers(displays: List<String>) {
        this.displays = displays.toMutableList()
        notifyDataSetChanged()
    }

    class DisplayViewHolder(
        private val binding: ItemDisplayBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(letter: String) {
            binding.tvLetter.text = letter
        }
    }
}