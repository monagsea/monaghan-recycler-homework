package com.example.myapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.ItemListBinding
import com.example.myapplication.view.ListFragmentDirections

class ListRecyclerAdapter: RecyclerView.Adapter<ListRecyclerAdapter.ListViewHolder>() {

    private var list = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val binding = ItemListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val listItem = list[position]
        holder.loadList(listItem)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addListItem(list: List<String>) {
        this.list = list.toMutableList()
        notifyDataSetChanged()
    }

    class ListViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadList(listItem: String) {
            binding.tvList.text =  listItem
            binding.tvList.setOnClickListener {
                it.findNavController().navigate(ListFragmentDirections.actionListFragmentToDisplayFragment(listItem))
            }
        }
    }
}